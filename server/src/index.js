const express = require('express');
const app = express();
const router = express.Router();
const swagger = require('./index.swagger');   
const reactApp = require('./index.reactapp');
var bodyParser = require('body-parser');
const { db, environment } = require('./config');
const listen = require('./index.listen');
const endpoints = require('./endpoints');
const { apiVersion } = require('./config');

const port = 3000;
const sequelize = require('./sequelize');

sequelize(()=> {
    console.log('DB READY');
  });
/**
 * Prevent server to crash when exception is thrown in production
 * */
if (environment === 'prod') {
  process.on('uncaughtException', (e) => displayError(e));
  process.on('unhandledRejection', (e) => displayError(e));
}
// Enable CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
  next();
});
// JSON Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Setup API routes for endpoints
endpoints(router, apiVersion); // load the v1 of the endpoints
app.use(`/${apiVersion}`, router);

// Initialize swagger
if (environment === 'local' || environment === 'dev') {
  console.log('--- SWAGGER INIT ---');
  swagger(app);
}

// Initialize Server
listen(app);
// Initialize the app static expose
reactApp(app);


// set the server to listen on port 3000
app.listen(port, () => console.log(`Listening on port ${port}`));
