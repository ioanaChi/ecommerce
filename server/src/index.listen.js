const http = require('http');
const https = require('https');
const fs = require('fs');
const { api } = require('./config');

const httpsOptions = {
  key: fs.readFileSync('./certs/key.pem'),
  cert: fs.readFileSync('./certs/cert.pem')
};

module.exports = function listen(app) {
  const server = api.ssl === true ? https.Server(httpsOptions, app) : http.Server(app);
  server.listen(api.port, function started() {
    const address = this.address();
    const host = address.address !== '::' ? address.address : 'localhost';
    const port = address.port;
    console.log('App listening at http://%s:%s', host, port);
    console.log('Swagger Doc started at http://%s:%s/docs', host, port);
  });
};

