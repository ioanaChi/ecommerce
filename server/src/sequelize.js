const appRoot = require('app-root-path');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const models = require('./models');
const { User } = require(`${appRoot}/src/models`);
const { Category } = require(`${appRoot}/src/models`);
const { Product } = require(`${appRoot}/src/models`);
const { db } = require('./config');

const { sequelizeDB } = models;

module.exports = (callback) => {
  
  function initSequelize(err) {
    console.log(`DB: `, db);
    if (err) {
      throw err;
    }
    if (db.wipe) {
      console.log(`-- Wiping existing database.`);
    }
  
    sequelizeDB
      .sync({ alter: true })
      .then(() => {
            User.bulkCreate([ 
              {
                name: `Ioana`,
                age: 12,
                email: `andrei@mcro-e.com`,
  
              }
              
            ])
            .then(function() {
              console.log('-- Database synced '.concat(db.wipe ? `: data it's wiped & schema recreated` : ``));
              if (callback) callback();
            });
            
      });
  }


  
  const connection = mysql.createConnection({
    host: db.host,
    port: db.port,
    user: db.username,
    password: db.password
  });
  connection.connect(function (err) {
    if(err) {
      console.log('error when connecting to db:', err);
    }     
  });
  connection.on('error', function(err) {
    throw err;
  });
  connection.query(`CREATE DATABASE IF NOT EXISTS ${db.database};`, initSequelize);
  connection.end();
  // add support for more dbs
};
