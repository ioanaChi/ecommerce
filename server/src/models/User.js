/**
 * User model
 * */
module.exports = (sequelize, DataTypes) =>
  sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      field: 'id',
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      field: 'name'
    },
    age: {
      type: DataTypes.INTEGER,
      field: 'age'
    },
    email: {
      type: DataTypes.STRING,
      field: 'email'
    },
    password:{
      type: DataTypes.STRING, 
      field: 'password'
    }
  });

