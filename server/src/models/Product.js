/**
 * Category model
 * */
module.exports = (sequelize, DataTypes) =>
  sequelize.define('Product', {
    img:{
      type: DataTypes.STRING,
      field: 'img'
    },
    id: {
      type: DataTypes.INTEGER,
      field: 'id',
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      field: 'name'
    },
    categoryId: {
      type: DataTypes.INTEGER,
      field: 'categoryId'
    },
   
  });
 

