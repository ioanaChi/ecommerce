/**
 * Category model
 * */
module.exports = (sequelize, DataTypes) =>
  sequelize.define('Category', {
    id: {
      type: DataTypes.INTEGER,
      field: 'id',
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      field: 'name'
    },
    parentId: {
      type: DataTypes.INTEGER,
      field: 'parentId'
    },
    description: {
      type: DataTypes.STRING,
      field: 'description'
    },
    subs: {
      type: DataTypes.STRING,
      get: function () {
        return JSON.parse(this.getDataValue('subs'));
      },
      set: function (val) {
        return this.setDataValue('subs', JSON.stringify(val));
      }
    },

  });

