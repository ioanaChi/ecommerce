const appRoot = require('app-root-path');
const { Category } = require(`${appRoot}/src/models`);
const { Product } = require(`${appRoot}/src/models`);
const Promise = require('bluebird');
Category.hasMany(Product, { foreignKey: 'categoryId' });
Product.belongsTo(Category, { foreignKey: 'categoryId' });
/**
 * Class that represents Categories orchestration trough database
 */

module.exports = {
  /**
   * Add a new category in database
   *
   * @param {Object} category - category JSON object
   */
  add(category) {

    return new Promise((resolve, reject) => {
      Category
        .create(category)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        })
    })
  },
  /**
 * List all categories from database
 *
 * @returns {Array}
 */
  list() {
    return new Promise((resolve, reject) => {
      // console.log("list");
      
      Category
        .findAll(
         
        )
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  listAll(obj) {
    return new Promise((resolve, reject) => {
      Category
        .findAll({
          where: {
            name: obj.name
          }
        })
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  /**
   * Get a specific category
   *
   * @param {integer} id - id
   * @returns {Object}
   */
  findById(id) {
    return new Promise((resolve, reject) => {
      Category
        .findOne({
          where: {
            id
          }
        })
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },


// intoarce tot tree-ul dupa id-ul parinte
  returnEverythingById(id) {

    return new Promise((resolve, reject) => {
      Category
        .findOne({
          where: {
            id
          }
        }).then((category) => {
          // console.log('category',category);
          
          
          const getCategories = (categ, allC) => {
            return new Promise((rez, rej) => {
              Category.findAll(
                {
                  where: {
                    parentId: categ.get('id')
                  }, 
                  include:[{
                    model: Product,
                    categoryId:id
                  }]
                })
                .then((categories, prods) => {
                  if (categories && categories.length > 0) {
                    categ.set('subs', categories);
                    const promises = [];
                    categories.forEach((cat) => {
                      promises.push(new Promise((rr, rj) => {
                        getCategories(cat).then((cats) => {
                          if (cats && cats[0]) {
                            cat.set('subs', cats);
                            cat.set('name', cats.name);
                          }
                          rr(cat);

                        });
                      }));
                    });
                    rez(Promise.all(promises))
                  } else {
                    categ.set('subs', []);
                    rez(categ);
                  } 
                })
                .catch(rej);
            });
          }
          getCategories(category).then((catt) => {
            category.set('subs', catt);
            resolve(catt);
          });
        });
    });
  },


  getAllForACategory(id) {
    var allChildren = [];
    return new Promise((resolve, reject) => {

      Category
        .findAll({
          where: {
            id:id
          }
        }).then((category) => {
          const getCategories = (categ, allC) => {
         
            return new Promise((rez, rej) => {
              Category.findAll(
                {
                  where: {
                    parentId: categ.get('id')
                  }, 
                  include:[{
                    model: Product,
                    categoryId:id
                  }]
                })
                .then((categories) => {

                  if (categories && categories.length > 0) {
                    allC = [];
                    allC.concat(categories);
                    categ.set('subs', categories);
                    const promises = [];
                    categories.forEach((cat) => {
                      promises.push(new Promise((rr, rj) => {
                        getCategories(cat, allC).then((cats) => {
                          if (cats && cats[0]) {
                            cat.set('subs', cats);
                          }
                          rr(cat);

                        });
                      }));
                    });
                    rez(Promise.all(promises))
                  } else {
                    categ.set('subs', []);
                    rez(categ);
                  }
                })
                .catch(rej);
            });
          }
          getCategories(category[0], allChildren).then((catt) => {
            
            category[0].set('subs', catt);
            resolve(category[0]);
          });
        }).catch((error) => {
          reject(error);
        });
    });
  },

  getAProductAndCat(idC, idP) {
    return new Promise((resolve, reject) => {
      
      Product
        .findAll({
          where:{
            id:idP,
            categoryId:idC
          },
          include:[Category],
        
        }).then((product) => {
        
         resolve(product);
         
        }).catch((error) => {
          reject(error);
        });
    });
  }, 


  getProductForCat(id) {
    return new Promise((resolve, reject) => {
      
      Product
        .findAll({
          where:{
            categoryId:id
          },
          include:[Category],
        
        }).then((product) => {
         resolve(product);
         
        }).catch((error) => {
          reject(error);
        });
    });
  }, 
  
  getAProductAndCat(idC, idP) {
  
    return new Promise((resolve, reject) => {
      
      Product
        .findAll({
          where:{
            id:idP,
            categoryId:idC
          },
          include:[Category],
        
        }).then((product) => {
        
         resolve(product);
         
        }).catch((error) => {
          reject(error);
        });
    });
  }, 
  

  /**
 * Removes a category from database 
 *
 * @param {integer} categoryId - category Id
 */
  remove(categoryId) {
    return new Promise((resolve, reject) => {
      Category
        .destroy({
          where: {
            id: categoryId
          }
        })
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  /**
   * Update a specific category on database
   *
   * @param {integer} name - category name
   * @param {Object} data - category object to update
   */
  update(name, data) {
    return new Promise((resolve, reject) => {
      Category.findOne({
        where: {
          name
        }
      }).then((category) => {
        if (category) {
          category
            .update(data)
            .then((res) => {
              resolve(res);
            })
            .catch((error) => {
              reject(error);
            });
        } else {
          reject({ error: 'Category name not found' });
        }
      }).catch((error) => {
        reject(error);
      });
    });
  },
};

