const { Category } = require('./controller');
const appRoot = require('app-root-path');

const tokenValidation = require(`${appRoot}/src/services/TokenService`);

/**
* @swagger
* definitions:
*   Categories:
*     type: object
*     required:
*       - name
*       - parentID
*       - description
*     properties:
*       name:
*         type: string
*       parentId:
*         type: integer
*       description:
*         type: string
*/
module.exports = (router) => {
  /**
   * @swagger
   * /category:
   *   post:
   *     category: return all categories
   *     description: all categories
   *     tags: 
   *       - Categories  
   *     produces:
   *       - routerlication/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "An object with name, descriptio  and parentId"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/categories"
   *     responses:
   *       200:
   *         description: "successful operation"
   *       400:
   *         description: "bad request"
   */
  router.post('/category', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);

    if (tokenValidity.success === true) {

      Category
        .listAll({
          name: req.body.name
        })
        .then((categories) => {

          if (categories.length > 0) {
            res.status(400).send({
              success: false,
              message: 'Category with this name already exists!'
            });
          } else {
            Category
              .add({
                name: req.body.name,
                parentId: req.body.parentId,
                description: req.body.description
              })
              .then((data) => {
                res.send({
                  success: true,
                  data
                });
              })
              .catch((error) => {
                res.status(400).send(error);
              });
          };
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
  router.get('/category/:id', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);

    if (tokenValidity.success === true) {
      Category
        .findById(req.params.id)
        .then((data) => {
          if (data <= 0) {
            res.sendStatus(404);
          } else {
            res.send({
              success: true,

              category: data
            });
          }
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
// returns all the subcategs and products for each category
  router.get('/getCategory/:id', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);
    if (tokenValidity.success === true) {
      Category
        .returnEverythingById(req.params.id)
        .then((data) => {
          // console.log(data);
          
          if (data <= 0) {
            res.sendStatus(404);
          } else {
            res.send({
              success: true,

              category: data
            });
          }
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {  
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });


  router.get('/Category/:id/Products', (req, res) => {

    const tokenValidity = tokenValidation.validateToken(req);
    if (tokenValidity.success === true) {
      Category
        .getProductForCat(req.params.id)
        .then((data) => {
          if (data <= 0) {
            res.sendStatus(404);
          } else {
            res.send({
              success: true,
              category: data
            });
          }
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
  router.get('/allForACategory/:id', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);
    if (tokenValidity.success === true) {
      Category
        .getAllForACategory(req.params.id)
        .then((data) => {
          console.log(data, 'dataaaaa');
          
          if (data <= 0) {
            res.sendStatus(404);
          } else {
            res.send({
              success: true,
              category: data
            });
          }
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });

  router.get('/Category/:id/Products/:id2', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);
    if (tokenValidity.success === true) {
      Category
        .getAProductAndCat(req.params.id, req.params.id2)
        .then((data) => {
          if (data <= 0) {
            res.sendStatus(404);
          } else {
            res.send({
              success: true,
              category: data
            });
          }
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
  router.get('/categories', (req, res) => {
    
    const tokenValidity = tokenValidation.validateToken(req);

    const name = req.body.name;
    const description = req.body.description;
    const parentId = req.body.parentId;
    if (tokenValidity.success === true) {
      
      Category
        .list()
        .then((data) => {
          
          res.send({
            success: true,
            data
          });

        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
  router.delete('/category/:id', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);
    if (tokenValidity.success === true) {

      Category
        .findById(req.params.id)
        .then((data) => {
          data.destroy();
          res.status(200).send({
            success: true,
            message: 'Category deleted.'
          });;
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
};
