const appRoot = require('app-root-path');
const { Product } = require(`${appRoot}/src/models`);
const Promise = require('bluebird');

/**
 * Class that represents Products orchestration trough database
 */

module.exports = {
  /**
   * Add a new Product in database
   *
   * @param {Object} product - Product JSON object
   */
  add(product) {
    
    return new Promise((resolve, reject) => {
      Product
        .create(product)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        })
    })
  },
    /**
   * List all Products from database
   *
   * @returns {Array}
   */
    list() {
    return new Promise((resolve, reject) => {
      Product
        .findAll()
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  listAll(obj) {
    return new Promise((resolve, reject) => {
      Product
        .findAll({
          where: {
            name: obj.name
          }
        })
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  /**
   * Get a specific Product
   *
   * @param {integer} id - id
   * @returns {Object}
   */
  findById(id) {
    return new Promise((resolve, reject) => {
      Product
        .findOne({
          where: {
            id
          }
        })
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
    /**
   * Removes an Product from database
   *
   * @param {integer} productId - Product Id
   */
  remove(productId) {
    return new Promise((resolve, reject) => {
      Product
        .destroy({
          where: {
            id: productId
          }
        })
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  /**
   * Update a specific Product on database
   *
   * @param {integer} name - Product name
   * @param {Object} data - Product object to update
   */
  update(name, data) {
    return new Promise((resolve, reject) => {
      Product.findOne({
        where: {
          name
        }
      }).then((product) => {
        if (product) {
          Product
            .update(data)
            .then((res) => {
              resolve(res);
            })
            .catch((error) => {
              reject(error);
            });
        } else {
          reject({ error: 'Product name not found' });
        }
      }).catch((error) => {
        reject(error);
      });
    });
  },
};

