const { Product } = require('./controller');
const appRoot = require('app-root-path');

const tokenValidation = require(`${appRoot}/src/services/TokenService`);

/**
* @swagger
* definitions:
*   Products:
*     type: object
*     required:
*       - name
*       - categoryId
*       - tags
*       - pictures
*       - price
*     properties:
*       name:
*         type: string
*       categoryId:
*         type: integer
*       tags:
*         type: string
*       price:
*         type: integer
*       pictures:
*         type: string
*/
module.exports = (router) => {
  /**
   * @swagger
   * /product:
   *   post:
   *     Product: return all Products
   *     description: all Products
   *     tags: 
   *       - Products  
   *     produces:
   *       - routerlication/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "An object with name, descriptio  and parentId"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/product"
   *     responses:
   *       200:
   *         description: "successful operation"
   *       400:
   *         description: "bad request"
   */
  router.post('/product', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);

    if (tokenValidity.success === true) {

      Product
        .listAll({
          name: req.body.name
        })
        .then((products) => {

          if (products.length > 0) {
            res.status(400).send({
              success: false,
              message: 'A product with this name already exists!'
            });
          } else {
            Product
              .add({
                name: req.body.name,
                categoryId: req.body.categoryId
                
              })
              .then((data) => {
                res.send({
                  success: true,
                  data
                });
              })
              .catch((error) => {
                res.status(400).send(error);
              });
          };
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
  router.get('/products', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);

    if (tokenValidity.success === true) {
      Product
        .list()
        .then((data) => {
          res.send({
            success: true,
            data
          });
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
  router.delete('/product/:id', (req, res) => {
    const tokenValidity = tokenValidation.validateToken(req);
    if (tokenValidity.success === true) {

      Product
        .findById(req.params.id)
        .then((data) => {
          data.destroy();
          res.status(200).send({
            success: true,
            message: 'Product deleted.'
          });;
        })
        .catch((error) => {
          res.status(400).send(error);
        });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  });
};
