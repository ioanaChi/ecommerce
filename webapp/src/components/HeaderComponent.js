
import React from 'react';
import { Link } from 'react-router-dom';
import auth from '../utils/authentication';
import { connect } from 'react-redux';
import "../scss/headerStyle.css";


const HeaderComponent = (props) => (

    <div style={{ height: 'auto', display: 'inline-block' }}>
        <Link className='signout' to="/login" onClick={() => { auth.clear() }}  >
            Sign Out
            </Link>
        <Link className='cartBtn' to="/cart">
        <div>
            <i className="fa fa-shopping-cart"></i>
<p style={props.cartProducts.length>0?{'visibility':'visible'}:{'visibility':'hidden'}} className='numberanimation' ><b>{props.cartProducts.length}</b></p>
</div>
        </Link>
    </div>
);
function mapStateToProps(state) {
    return {
        cartProducts: state.cartProducts
    }
}

export default connect(mapStateToProps)(HeaderComponent);
           