 const required = value => (value || typeof value === 'number' ? undefined : 'Required')


 const number = value =>
  value && isNaN(Number(value)) ? 'Must be a number' : undefined;

 const minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined;

   const minValue13 = minValue(13);

   const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined;

     const tooYoung = value =>
  value && value < 13
    ? 'You do not meet the minimum age requirement!'
    : undefined;

     const aol = value =>
  value && /.+@aol\.com/.test(value)
    ? 'Really? You still use AOL for your email?'
    : undefined;

     const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Only alphanumeric characters'
    : undefined;

     const passwordsMatch = (value, allValues) => 
    value !== allValues.password ? 'Passwords don\'t match' : undefined;
     const minLength = min => value =>
  value && min && value.length < min ? `Must be ${min} characters or more` : undefined;
const minLength3 = minLength(3)
  export {
    required,
    email,
    number,
    passwordsMatch,
    minLength,
    minValue,
    minValue13,
    alphaNumeric,
    aol,
    tooYoung,
     minLength3
  };