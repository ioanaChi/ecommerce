import React from 'react';
import axios from 'axios';

function callApi(config) {
    return new Promise((resolve, reject) => {
      axios(config)
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject(e.response);
        });
    });
  }


loginUserRequest = (email, password) => {
    const object = {
      email,
      password
    };
  
     return callApi({
      url: '/login',
      method: 'post',
      data: JSON.stringify(object)
    });
    /*return new Promise((resolve, reject) => {
      resolve({ status: 200, user: { name: 'Alexandru Lazar', email: 'alex@mcro-e.com' } });
      if (object === null) {
        reject({ message: 'nothing' });
      }
    });*/
  };
  
  