import LoginForm from './LoginForm';
import { loginAction } from '../../actions/submit'
import {bindActionCreators} from "redux";
import { connect} from 'react-redux';

function mapStateToProps(state) {
    return {
      UserLogin: state.UserLogin
    }
  }
  function dispatchStateToProps(dispatch) {
    return bindActionCreators({
      loginAction: loginAction   
    }, dispatch);
  }
  export default connect(mapStateToProps, dispatchStateToProps)(LoginForm);
