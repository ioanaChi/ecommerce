import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { required, email, minLength3 } from "../../config";
import { loginUser } from '../../utils/commonFunctionality'
import auth from '../../utils/authentication'
import { Link } from 'react-router-dom';
import "../../scss/registerStyle.css";



const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
    <div>
      <label>{label}</label>
      <div>
        <input {...input} placeholder={label} type={type} />
        {touched && error && <span>{error}</span>}
      </div>
    </div>
  )

class LoginForm extends Component {

  onSuccess = values => {
    const { history } = this.props;
    loginUser(values.email, values.password)
      .then(response => {
        auth.set({
          token: response.data.token,
          userId: response.data.user.id
        });

        history.push('/categories');
      });
  }

  render() {
    const { error, handleSubmit, pristine, reset, submitting, history } = this.props;

    return (
      <div className="registerdiv">
        <form name="Login" onSubmit={handleSubmit(this.onSuccess)} className="registerform">

          <Field
            name="email"
            type="email"
            component={renderField}
            className='registerinput'
            label="Email"
            validate={[required, email]}

          />
          <Field
            name="password"
            type="password"
            component={renderField}
            className="registerinput"
            label="Password"
            validate={[required, minLength3]}
          />

          {error && <strong>{error}</strong>}
          <div>
            <button type="submit" className="registerbutton" disabled={submitting}>
              Log In
            </button>
            <button type="button" disabled={pristine || submitting} onClick={reset}>
              Clear Values
            </button>
            <Link to="/register">
              Register
            </Link>
          </div>
        </form>
      </div>

    )

  };
};

export default reduxForm({
  form: 'LoginForm'
})(LoginForm);