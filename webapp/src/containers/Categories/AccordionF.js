import React, { Component } from 'react';
import classnames from 'classnames';

export default class AccordionF extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            height: 0
        };
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        // do things with nextProps.someProp and prevState.cachedSomeProp
        console.log(prevState, nextProps, "" );
        
        return {
            childrens: nextProps.childrens,
        
        };
      }
      shouldComponentUpdate(){
          
      }


    handleClick = (evt) => {
        evt.preventDefault();
        evt.stopPropagation();
        
        const {childrens, products} = this.props;
        const compHieght = ((childrens ? childrens.length : 0 + products? products.length : 0) * 32);
        this.setState(
            {
                isOpen: !this.state.isOpen,
                height: compHieght
            }
        );
    }
    render() {
        let child = [];
        let products = [];
        let compProducts = this.props.products ? this.props.products : [];
        if (this.props.childrens) {
            const allChildren = this.props.childrens;
            compProducts = compProducts.map((prodItem, prodIndex) => {
                return (<div key={prodIndex}>{prodItem.name}</div>);
            });


            if (Array.isArray(allChildren)) {
                allChildren.forEach((item, index) => {
                    if (item.subs.length > 0) {
                        child.push(<Accor key={index} titleTxt={item.name} childrens={item.subs} products={item.Products} />);
                    } else {
                        child.push(<Accor key={index} titleTxt={item.name} />);
                    }
                });
            }
        }

        if (this.state.isOpen === true && this.state.height > 0) {
            setTimeout(() => {
                this.setState({
                    height: 'auto'
                });
            }, 350);
        }
        if (this.state.isOpen === false && this.state.height > 0) {
            setTimeout(() => {
                this.setState({
                    height: 0
                });
            });
        } 
        return (
            <div className='custom'>
                <div className='accor-title' onClick={this.handleClick}>{this.props.titleTxt}</div>
                <div className='accor-body' style={{ height: this.state.height }}>
                    {compProducts}
                    {child}
                </div>
            </div>
        ) 
    }
}
// style={{height: (child.length+compProducts.length+32)*32}}
