import CategoriesView from './CategoriesView';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setFirstCategoryAction, setAllCategoriesAction, cartAction } from '../../actions/submit';

function mapStateToProps(state) {
    return {
        allCategories: state.allCategories,
        cartProducts: state.cartProducts
    }   
}
function dispatchStateToProps(dispatch) {
    return bindActionCreators({
        setFirstCategoryAction:setFirstCategoryAction,
        setAllCategoriesAction:setAllCategoriesAction,
        cartAction: cartAction 
    }, dispatch);
}
export default connect(mapStateToProps, dispatchStateToProps)(CategoriesView)