import React, { Component } from 'react';
import HeaderComponent from '../../components/HeaderComponent';
import { getSubCategs, returnFirstCategory } from '../../utils/commonFunctionality';
import Accordion from "./Accordion";
import Accor from "./Accor";
import "../../scss/categoriesStyle.css";
import "../../scss/accordion.css";

export default class CategoriesView extends Component {

    componentDidMount() {
        this.getCategories();
    }

    getCategories = () => {
        const tThis = this;
        if (this.props.allCategories.length === 0)
            returnFirstCategory()
                .then(response => {
                    let responseData = response.data.data;
                    let firstCateg = [];
                    responseData.forEach((item, index) => {
                        if (item.parentId == null) {
                            firstCateg.push(item);
                            getSubCategs(item.id)
                                .then(response2 => {
                                    tThis.props.setAllCategoriesAction([...tThis.props.allCategories, response2.data.category]);
                                })
                        }
                    });
                })
                .catch(error => console.log(error))
    }

    // sendProdToCart = (prod) => {
    //     if (Array.isArray(this.props.cartProducts)) {
    //         if (this.props.cartProducts.indexOf(prod) === -1) {
    //             console.log('pppppp');
                
    //             // this.props.cartAction([...this.props.cartProducts, prod]);
    //         }
    //     }
    // }


    // displaySubcateg = (arr2) => {
    //     if (arr2 && arr2.length > 0) {
    //         return arr2.map((cat, index) => {
    //             let allCh = []
    //             if (cat && cat.hasOwnProperty('Products') && cat.Products.length > 0) {
    //                 allCh = allCh.concat(cat.Products);
    //                 allCh = allCh.concat(cat.subs);
    //                 return (<Accordion className="accordion" key={index}>
    //                     <div data-header={cat.name} className="accordion-item" >
    //                         {this.displaySubcateg(allCh)}
    //                     </div>
    //                 </Accordion>)
    //             } else {
    //                 if (cat.hasOwnProperty('categoryId')) {
    //                     return (<p className="productname" key={index} >
    //                         {cat.name}
    //                         <button className="addbtn" onClick={() => this.sendProdToCart(cat)}>Add to cart</button>
    //                     </p>)
    //                 }
    //                 return (<Accordion className="accordion" key={index} >
    //                     <div data-header={cat.name} key={index} className="accordion-item" >
    //                         {this.displaySubcateg(cat.subs)}
    //                     </div>
    //                 </Accordion>)
    //             }
    //         })
    //     } else {
    //         return [];
    //     }
    // };
    // displayFirstChildren = (arr) => {
    //     return arr.map(fcat => {
    //         return (<Accordion className="accordion" key={fcat.id} >
    //             <div data-header={fcat.name} className="accordion-item">
    //                 {this.displaySubcateg(fcat.subs)}
    //             </div>
    //         </Accordion>)
    //     });
    // };
    displayArr = (arr) => {
        
       return arr.map((x, index) => {
            return (
                <Accor props={this.props} key={index} titleTxt={x['name']} childrens={x['subs']} />
            );
        });
    }

    render() {
        const allCategories = Array.isArray(this.props.allCategories) ? this.props.allCategories : [];
        return (
            <div>
                <div className="container">
                    <div className='row'>
                        <div className='col-lg-12 acc-div'>
                            < HeaderComponent />
                            {this.displayArr(allCategories)}
                        </div>
                    </div>
                </div>
            </div>
        )

        return (< div className="categcontainingdiv"  >
            < HeaderComponent />
            {this.displayFirstChildren(allCategories)}
        </div>)
    }
}


