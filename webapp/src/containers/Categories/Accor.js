import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { cartAction } from '../../actions/submit';

class Accor extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isOpen: false,
            height: 0
        };
        this.sendProdToCart = this.sendProdToCart.bind(this);

    }

    sendProdToCart = (prod, action, prods) => {
        console.log(action, prods, "hhh", );

        if (Array.isArray(prods)) {
            console.log('hhy');

            if (prods.indexOf(prod) === -1) {
                action([...prods, prod]);
            }
        }
    }

    handleClick = (evt) => {
        evt.preventDefault();
        evt.stopPropagation();
        const { childrens, products } = this.props;
        const compHieght = (((childrens ? childrens.length : 0) + (products ? products.length : 0)) * 32);

        this.setState(
            {
                isOpen: !this.state.isOpen,
                height: compHieght
            }
        );
    }
    render() {
        const tThis = this;

        console.log(this.props.cartProducts, tThis.props, 'arrrrrrx');

        let child = [];
        let products = [];
        let compProducts = this.props.products ? this.props.products : [];

        const cartAction = tThis.props.cartAction?tThis.props.cartAction:null;
            const cartProducts = tThis.props.cartProducts?tThis.props.cartProducts:null;

        if (this.props.childrens) {
            console.log('intra');

            const allChildren = tThis.props.childrens;

            compProducts = compProducts.map((prodItem, prodIndex) => {
            

                return (<div className='product' key={prodIndex}>{prodItem.name}<button onClick={(event) => {

                    tThis.sendProdToCart(prodItem, cartAction,cartProducts);
                }} className='addButton'>Add to cart</button></div>);
            });
            console.log('ssdd');


            if (Array.isArray(allChildren)) {
                allChildren.forEach((item, index) => {
                    const newProps = tThis.props ? tThis.props : null;
                    if (item.subs.length > 0) {
                        child.push(<Accor cartAction={cartAction} cartProducts={cartProducts} key={index} titleTxt={item.name} childrens={item.subs} products={item.Products} />);
                    } else {
                        child.push(<Accor  cartAction={cartAction} cartProducts={cartProducts} key={index} titleTxt={item.name} />);
                    }
                });
            }
        }

        if (this.state.isOpen === true && this.state.height > 0) {
            setTimeout(() => {
                this.setState({
                    height: 'auto'
                });
            }, 350);
        }
        if (this.state.isOpen === false && this.state.height > 0) {
            setTimeout(() => {
                this.setState({
                    height: 0
                });
            });
        }


        return (
            <div className='custom'>
                <div className={this.state.isOpen > 0 ? 'accor-title activeTitle' : 'accor-title'} onClick={this.handleClick}>{this.props.titleTxt}<i className="fa fa-angle-right"></i></div>
                <div className='accor-body' style={{ height: this.state.height }}>
                    {compProducts}
                    {child}
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    console.log('state.cartProducts', state.cartProducts);

    return {
        cartProducts: state.cartProducts
    };
};

function dispatchStateToProps(dispatch) {
    return bindActionCreators({
        cartAction: cartAction
    }, dispatch);
}
export default connect(mapStateToProps, dispatchStateToProps)(Accor);