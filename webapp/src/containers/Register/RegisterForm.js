import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { passwordsMatch, required, alphaNumeric, email, aol, number, minValue13, minLength, tooYoung } from "../../config";
import { registerUser } from '../../utils/commonFunctionality';
import { registerAction } from '../../actions/submit'

import "../../scss/registerStyle.css";

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
    <div className="RegisterLabel">
      <label>{label}</label>
      <div
        className="registerinput">
        <input {...input} placeholder={label} type={type} />
        {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  )
class RegisterForm extends Component {


 onRequest = val => {
  const { history } = this.props;
   
    registerUser(val.username, val.age, val.email, val.password)
      .then(response => {
        registerAction(response.data.data);
        history.push('/categories');
      })
      .catch((error) => console.log(error.response));
}
  render() {
    const {handleSubmit,pristine,history,reset,submitting, registerAction,} = this.props;
    return (
      <div className="registerdiv">
        <form onSubmit={handleSubmit(this.onRequest)} className="registerform">
          <Field
            name="username"
            type="text"
            className="registerinput"
            component={renderField}
            label="Username"
            validate={[required]}
            warn={alphaNumeric}
          />

          <Field
            name="email"
            type="email"
            className="registerinput"
            component={renderField}
            label="Email"
            validate={[required, email]}
            warn={aol}
          />

          <Field
            name="age"
            type="number"
            className="registerinput"
            component={renderField}
            label="Age"
            validate={[required, number, minValue13]}
            warn={tooYoung}
          />

          <Field
            name="password"
            type="password"
            component={renderField}
            label="Password"
            validate={[required, minLength(3)]}
          />

          <Field
            name="password2"
            type="password"
            component={renderField}
            className="registerinput"
            label="Password Verification"
            validate={[required, passwordsMatch]}
          />
          <div>
            <button type="submit" disabled={submitting} className="registerbutton" >
              Submit
        </button>
            <button type="button" disabled={pristine || submitting} onClick={reset}>
              Clear Values
        </button>
            <Link to="/login" >
              Back to Login
        </Link>
          </div>
        </form>
      </div>
    )
  }
}

export default reduxForm({
  form: 'RegisterForm' // a unique identifier for this form
})(RegisterForm)





