import RegisterForm from './RegisterForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { registerAction } from '../../actions/submit'


function mapStateToProps(state) {
    return {
        userData: state.UserRegister
    }
}
function dispatchStateToProps(dispatch) {
    return bindActionCreators({
        registerAction: registerAction
    }, dispatch)
}

export default connect(mapStateToProps, dispatchStateToProps)(RegisterForm);