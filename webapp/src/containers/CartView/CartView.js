import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../scss/cartView.css';
import HeaderComponent from '../../components/HeaderComponent';



export default class CartView extends Component {
    removeProduct = (id, index) => {
        this.props.cartProducts.splice(index, 1);
        this.props.cartAction([...this.props.cartProducts]);
    }


    displayProducts = (prodsArr) => {
        if (prodsArr.length > 0) {
            return prodsArr.map((prod, index) => {
                return (<tr key={index}>
                    <td>{prod.id}</td>
                    <td >{prod.name} <button onClick={() => this.removeProduct(prod.id, index)} className='btnremove' >Remove</button></td>
                </tr>)
            })
        } else {
            return
        }
    }
    render() {
        const { cartProducts } = this.props;
        let table;
        if (cartProducts.length > 0) {
            table = (<table className='tableprodselected'>
                <thead>
                    <tr className='tablehead'>
                        <th>ID</th>
                        <th>Product</th>
                    </tr>
                </thead>
                <tbody>
                    {this.displayProducts(cartProducts)}
                </tbody>
            </table>)
        } else {
            table = (<p className='noprod-selected'>There are no products selected</p>)
        }
        return (<div className='cartcontainingdiv'>

            <HeaderComponent />
            <Link to='/categories' className='backToCat'><i className="backToCat fa fa-arrow-left"></i>
                Back To Categories
            </Link>

            <p className='tablename'>Products Selected</p>  
            {table}
        </div>)
    }
}