import CartView from './CartView';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { cartAction } from '../../actions/submit';

function mapStateToProps(state) {
    return {
        cartProducts: state.cartProducts
    }
}
function dispatchStateToProps(dispatch) {

    return bindActionCreators({
        cartAction: cartAction
    }, dispatch)
}

export default connect(mapStateToProps, dispatchStateToProps)(CartView)