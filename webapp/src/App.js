import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import LoginForm from "./containers/Login";
import RegisterForm from "./containers/Register";
import CategoriesView from "./containers/Categories/index";
import CartView from "./containers/CartView/index";
import Accor from "./containers/Categories/Accor";
import auth from './utils/authentication';
import { verifyAuth } from './utils/commonFunctionality'
import { history } from './utils/history';
import  '../node_modules/bootstrap/dist/css/bootstrap.min.css';

class App extends Component {

  componentDidMount() {
    const storageUserData = auth.get();
    if (storageUserData.token) {
      //verifying if the token is valid
      verifyAuth(storageUserData.token)
        .then(response => {
          if (response.data.success) {
            history.push('/categories');
          }
        })
        .catch(() => {
          history.push('/login');
        })
    } else {
      if (history.location.pathname !== '/register')
        history.push('/login');
    };
  }
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route exact key="login" path="/login" component={LoginForm} />
          <Route key="register" path="/register" component={RegisterForm} />
          <Route path="/cart" component={CartView} />
          <Route path="/categories" component={CategoriesView} />
          <Route path="/accor" component={Accor} />
        </Switch >
      </Router>
    )
  }
}
function mapStateToProps(state) {
  return {
    UserLogin: state.UserLogin,
    UserRegister: state.UserRegister
  }
}
export default connect(mapStateToProps)(App);
