import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import allReducers from './reducers';
import './index.css';

import '../node_modules/font-awesome/css/font-awesome.min.css';
const store = createStore(allReducers, applyMiddleware(thunk));



const RoutingApp = ({ store }) => (
  <Provider store={store}>
    <App />
  </Provider>
)

render(
  <RoutingApp store={store} />,
  document.getElementById("root")
);
registerServiceWorker();

