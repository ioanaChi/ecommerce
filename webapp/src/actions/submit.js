const loginAction = (data) => {
    return {
        type: 'LOGIN_ACTION',
        payload: data
    }
};
const registerAction = (data) => {
    return {
        type: 'REGISTER_ACTION',
        payload: data
    }
};
const setFirstCategoryAction = (data) => {
    
    return {
        type: 'SET_FIRSTCATEG_ACTION',
        payload: data
    }
};
const setAllCategoriesAction = (data) =>{
    return {
        type:'SET_ALL_CATEGORIES_ACTION',
        payload :data
    }
}
const cartAction = (data) => {
    return {
        type:'SET_CART_PRODUCTS',
        payload: data
    }
}

export {
    cartAction,
    loginAction,
    registerAction,
    setFirstCategoryAction,
    setAllCategoriesAction
}