import { combineReducers } from 'redux';
import firstCategory from './firstCategory';
import UserLogin from './UserLogin'
import UserRegister from './UserRegister'
import allCategories from './allCategories';
import cartProducts from './cartProducts';
import { reducer as form } from 'redux-form';



const allReducers = combineReducers({
    cartProducts,
    firstCategory,
    allCategories,
    form,
    UserLogin,
    UserRegister
})
export default allReducers;