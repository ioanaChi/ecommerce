const TOKEN_KEY = "Ecommerce Authorization";
const USERID_KEY = "UserId";
const Storage = localStorage;



export default {
    get() {
        return {
            token: Storage.getItem(TOKEN_KEY),
            userId: Storage.getItem(USERID_KEY)
        }
    },

    set(data) {
        Storage.setItem(TOKEN_KEY, data.token),
        Storage.setItem(USERID_KEY, JSON.stringify(data.userId))
    },

    clear() {
        Storage.removeItem(TOKEN_KEY),
        Storage.removeItem(USERID_KEY)
    }
}