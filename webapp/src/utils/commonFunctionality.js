import axios from 'axios';
import auth from './authentication'




function verifyAuth(tokenToVerif) {
    return axios({
        url: 'http://localhost:3000/v1/auth',
        method: 'post',
        data: {
            token: tokenToVerif
        }
    });
};



function loginUser(emailLogin, passwordLogin) {

    return axios({
        url: 'http://localhost:3000/v1/login',
        method: 'post',
        data: {
            password: passwordLogin,
            email: emailLogin
        }
    });
};


function registerUser(nameUser, ageUser, mailUser, passwordUser) {

    return axios({
        url: 'http://localhost:3000/v1/register',
        method: 'post',
        data: {
            name: nameUser,
            age: ageUser,
            password: passwordUser,
            email: mailUser
        }
    });
};
function returnCategories(id) {
    return axios.get('http://localhost:3000/v1/getCategory/'+id, {
        headers: {
            Authorization: 'Bearer ' + auth.get().token,
            'Content-type': 'x-auth-token'
        }
    })
}
function returnFirstCategory(){
    return axios.get('http://localhost:3000/v1/categories',{
        headers: {
            Authorization: 'Bearer ' + auth.get().token,
            'Content-type': 'x-auth-token'
        }
    })
}

function getSubCategs(id){
    
    return axios.get('http://localhost:3000/v1/allForACategory/'+id,{
        headers: {
            Authorization: 'Bearer ' + auth.get().token,
            'Content-type': 'x-auth-token'
        }
    })
    }





export {
    loginUser,
    registerUser,
    verifyAuth,
    returnCategories,
    getSubCategs,
    returnFirstCategory
}